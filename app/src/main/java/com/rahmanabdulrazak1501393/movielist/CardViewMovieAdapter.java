package com.rahmanabdulrazak1501393.movielist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class CardViewMovieAdapter extends RecyclerView.Adapter<CardViewMovieAdapter.CardViewViewHolder>{

    private ArrayList<Movie3> listMovie;
    private Context context;

    CardViewMovieAdapter(Context context) {
        this.context = context;
    }

    private ArrayList<Movie3> getListMovie() {
        return listMovie;
    }

    void setListMovie(ArrayList<Movie3> listMovie) {
        this.listMovie = listMovie;
    }

    @Override
    public CardViewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cardview_movie, parent, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardViewViewHolder holder, int position)
    {
        Movie3 p = getListMovie().get(position);
        Glide.with(context)
                .load(p.getPhoto())
                .override(350, 550)
                .into(holder.imgPhoto);

        holder.tvName.setText(p.getName());
        holder.tvRemarks.setText(p.getRemarks());

        holder.btnFavorite.setOnClickListener((View.OnClickListener) new
                CustomOnItemClickListener(position, new
                CustomOnItemClickListener.OnItemClickCallback() {
                    @Override
                    public void onItemClicked(View view, int position) {
                        Toast.makeText(context, "Like "+getListMovie().get(position).getName(), Toast.LENGTH_SHORT).show();
                    }
                }));
        holder.btnShare.setOnClickListener((View.OnClickListener) new
                CustomOnItemClickListener(position, new
                CustomOnItemClickListener.OnItemClickCallback() {
                    @Override
                    public void onItemClicked(View view, int position) {
                        Toast.makeText(view.getContext(), "Recycle Click " + position, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(view.getContext(), DetailMovie.class);
                        intent.putExtra("INDEX", position);
                        context.startActivity(intent);
                    }
                }));
    }


    @Override
    public int getItemCount() {
        return getListMovie().size();
    }

    class CardViewViewHolder extends RecyclerView.ViewHolder{
        ImageView imgPhoto;
        TextView tvName, tvRemarks;
        Button btnFavorite, btnShare;
        CardViewViewHolder(View itemView) {
            super(itemView);
            imgPhoto =
                    (ImageView)itemView.findViewById(R.id.img_item_photo);
            tvName = (TextView)itemView.findViewById(R.id.tv_item_name);
            tvRemarks =
                    (TextView)itemView.findViewById(R.id.tv_item_remarks);
            btnFavorite =
                    (Button)itemView.findViewById(R.id.btn_set_favorite);
            btnShare = (Button)itemView.findViewById(R.id.btn_set_share);
        }
    }
}

