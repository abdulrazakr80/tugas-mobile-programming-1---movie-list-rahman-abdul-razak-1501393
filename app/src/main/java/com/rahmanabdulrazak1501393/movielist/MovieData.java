package com.rahmanabdulrazak1501393.movielist;

import android.graphics.Movie;

import java.util.ArrayList;
import java.util.Collection;

public class MovieData {

    public static String[][] data = new String[][]{
            {"FIRST MAN", "Biography, Drama, History", "http://media.21cineplex.com/webcontent/gallery/pictures/153692796821512_452x647.jpg" },
            {"REPRISIAL", "Action, Crime, Thriller", "http://media.21cineplex.com/webcontent/gallery/pictures/153501880827862_300x430.jpg" },
            {"VENOM", "Action, Horror, Sci-fi ", "http://media.21cineplex.com/webcontent/gallery/pictures/153726067590660_452x647.jpg" },
            {"HALLOWEN", "Horror, Thriller", "http://media.21cineplex.com/webcontent/gallery/pictures/153777968171325_300x430.jpg" },
            {"GOOSEBUMPS 2 : HAUNTED HALLOWEEN", "Adventure, Comedy, Family", "http://media.21cineplex.com/webcontent/gallery/pictures/153612216371481_300x430.jpg" },
            {"ASIH", "Horror", "http://media.21cineplex.com/webcontent/gallery/pictures/153793694240829_300x430.jpg" },
            {"L STORM", "Action", "http://media.21cineplex.com/webcontent/gallery/pictures/153742743558643_300x430.jpg" },
            {"SUZZANNA: BERNAPAS DALAM KUBUR", "Horror", "http://media.21cineplex.com/webcontent/gallery/pictures/153958943058191_300x430.jpg" },
            {"DORAEMON", "Animation , Adventure , Comedy", "http://media.21cineplex.com/webcontent/gallery/pictures/153958834362114_300x430.jpg" },
            {"A SIMPLE FAVOR", "Thriller", "http://media.21cineplex.com/webcontent/gallery/pictures/15385419577937_300x430.jpg"}
    };

    public static Collection<? extends Movie3> getListData(){
        Movie3 movie3 = null;
        ArrayList<Movie3> list = new ArrayList<>();
        for (String[] aData : data) {
            movie3 = new Movie3();
            movie3.setName(aData[0]);
            movie3.setRemarks(aData[1]);
            movie3.setPhoto(aData[2]);
            list.add(movie3);
        }
        return list;
    }


}





