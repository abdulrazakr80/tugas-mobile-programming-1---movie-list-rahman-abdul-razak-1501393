package com.rahmanabdulrazak1501393.movielist;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class GridMovieAdapter extends RecyclerView.Adapter<GridMovieAdapter.GridViewHolder> {

    private Context context;

    private ArrayList<Movie3> listMovie;

    private ArrayList<Movie3> getListMovie() {
        return listMovie;
    }

    void setListMovie(ArrayList<Movie3> listMovie) {
        this.listMovie = listMovie;
    }

    GridMovieAdapter(Context context) {
        this.context = context;
    }

    @Override
    public GridViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grid_movie, parent, false);
        return new GridViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GridViewHolder holder, final int position) {
        Glide.with(context)
                .load(getListMovie().get(position).getPhoto())
                .override(350, 550)
                .into(holder.imgPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Recycle Click " + position, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(view.getContext(), DetailMovie.class);
                intent.putExtra("INDEX", position);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return getListMovie().size();
    }

    class GridViewHolder extends RecyclerView.ViewHolder{
        ImageView imgPhoto;
        GridViewHolder(View itemView) {
            super(itemView);
            imgPhoto =
                    (ImageView)itemView.findViewById(R.id.img_item_photo);
        }
    }
}

